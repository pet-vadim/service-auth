package auth

import (
	"context"
	"github.com/asaskevich/govalidator"
	"gitlab.com/pet-vadim/libs/errs"
	"unicode"
	"unicode/utf8"
)

func (s *srv) SingUp(ctx context.Context, login, password string) (refreshToken, accessToken string, err *errs.AppError) {
	validateErr := Validate(login, password)
	if validateErr != "" {
		return "", "", errs.NewValidationError(validateErr)
	}

	passwordHash, appErr := HashPassword(password)
	if appErr != nil {
		return "", "", appErr
	}

	user, appErr := s.repo.CreateUser(ctx, login, passwordHash)
	if appErr != nil {
		return "", "", appErr
	}

	refreshToken, accessToken, appErr = s.token.Create(ctx, user.UserID)
	if appErr != nil {
		return "", "", appErr
	}

	return refreshToken, accessToken, nil
}

//  Вынесено для тестов
const (
	EmailValidationErr = "invalid email"
	PassValidationErr  = "password should contain at least: one upper case english letter, one" +
		" lower case english letter, one digit, one special character, eight symbols"
)

func Validate(login, password string) string {
	validationErr := getPasswordValidationErr(password)
	if validationErr != "" {
		return validationErr
	}

	if !govalidator.IsEmail(login) {
		return EmailValidationErr
	}

	return ""
}

func getPasswordValidationErr(password string) string {
	passLength := utf8.RuneCountInString(password)
	if passLength < 7 || passLength > 50 {
		return PassValidationErr
	}

	var isNumber, isUpper, isSpecial bool

	letters := 0
	for _, c := range password {
		switch {
		case unicode.IsNumber(c):
			isNumber = true
		case unicode.IsUpper(c):
			isUpper = true
			letters++
		case unicode.IsPunct(c) || unicode.IsSymbol(c):
			isSpecial = true
		case unicode.IsLetter(c) || c == ' ':
			letters++
		default:
			// Do nothing
		}
	}

	if !isNumber || !isUpper || !isSpecial {
		return PassValidationErr
	}

	// Password valid
	return ""
}
