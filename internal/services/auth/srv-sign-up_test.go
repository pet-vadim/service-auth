package auth_test

import (
	"context"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/repositories/auth"
	mock_token "gitlab.com/pet-vadim/service-auth/mocks/service/token"
	"testing"
)

func Test_srv_SingUp(t *testing.T) {
	type setMockToken func(s *mock_token.MockService, userID, refreshToken, accessToken string)
	type setMockAuthRepo func(s *mock_auth.MockRepository, login, passwordHash, userID string)
	tests := []struct {
		name            string
		isSuccess       bool
		userID          string
		login           string
		password        string
		errMsg          string
		refreshToken    string
		accessToken     string
		setMockToken    setMockToken
		setMockAuthRepo setMockAuthRepo
	}{
		{
			name:         "succes",
			isSuccess:    true,
			userID:       "123",
			login:        "test@mail.ru",
			password:     "Password1@",
			errMsg:       "",
			refreshToken: "refresh.jwt.token",
			accessToken:  "access.jwt.token",
			setMockToken: func(s *mock_token.MockService, userID, refreshToken, accessToken string) {
				s.
					EXPECT().
					Create(context.Background(), userID).
					Return(refreshToken, accessToken, nil).
					MinTimes(0)
			},
			setMockAuthRepo: func(s *mock_auth.MockRepository, login, passwordHash, userID string) {
				s.
					EXPECT().
					CreateUser(context.Background(), login, gomock.Any()).
					Return(auth.User{
						UserID:       userID,
						Email:        login,
						PasswordHash: passwordHash,
					}, nil)
			},
		},
		{
			name:         "fail validation email",
			isSuccess:    false,
			userID:       "123",
			login:        "test@mailru",
			password:     "Password1@",
			errMsg:       auth.EmailValidationErr,
			refreshToken: "",
			accessToken:  "",
			setMockToken: func(s *mock_token.MockService, userID, refreshToken, accessToken string) {
				s.
					EXPECT().
					Create(context.Background(), userID).Return(refreshToken, accessToken, nil).
					MinTimes(0)
			},
			setMockAuthRepo: func(s *mock_auth.MockRepository, login, passwordHash, userID string) {
				s.
					EXPECT().
					CreateUser(context.Background(), login, gomock.Any()).
					Return(auth.User{
						UserID:       userID,
						Email:        login,
						PasswordHash: passwordHash,
					}, nil).
					MinTimes(0)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctr := gomock.NewController(t)
			ctr.Finish()

			repo := mock_auth.NewMockRepository(ctr)
			token := mock_token.NewMockService(ctr)
			tt.setMockToken(token, tt.userID, tt.refreshToken, tt.accessToken)
			tt.setMockAuthRepo(repo, tt.login, "any", tt.userID)
			srv := auth.New(repo, token)

			refreshToken, accessToken, appErr := srv.SingUp(context.Background(), tt.login, tt.password)

			assert.Equal(t, tt.refreshToken, refreshToken)
			assert.Equal(t, tt.accessToken, accessToken)
			if tt.errMsg == "" {
				assert.Equal(t, nil, appErr)
			} else {
				assert.Equal(t, tt.errMsg, appErr.Error())
			}
		})
	}
}

func Test_validate(t *testing.T) {
	tests := []struct {
		name     string
		login    string
		password string
		want     string
	}{
		{
			name:     "success",
			login:    "test@mail.ru",
			password: "Password1@",
			want:     "",
		},
		{
			name:     "invalid email",
			login:    "test@mailru",
			password: "Password1@",
			want:     auth.EmailValidationErr,
		},
		{
			name:     "invalid password (no numbers)",
			login:    "test@mail.ru",
			password: "Password@",
			want:     auth.PassValidationErr,
		},
		{
			name:     "invalid password (no spec symbols)",
			login:    "test@mail.ru",
			password: "Password1",
			want:     auth.PassValidationErr,
		},
		{
			name:     "invalid password (no capital letters)",
			login:    "test@mail.ru",
			password: "password@1",
			want:     auth.PassValidationErr,
		},
		{
			name:     "invalid password (length < 7)",
			login:    "test@mail.ru",
			password: "Paaa@1",
			want:     auth.PassValidationErr,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := auth.Validate(tt.login, tt.password); got != tt.want {
				t.Errorf("Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
