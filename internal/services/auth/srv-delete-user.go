package auth

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
)

func (s *srv) DeleteUser(ctx context.Context, login, password string) *errs.AppError {
	user, err := s.repo.GetUser(ctx, login)
	if err != nil {
		return err
	}

	if !CheckPasswordHash(password, user.PasswordHash) {
		logger.Info("fail check password hash")
		return errs.NewAuthorizationError("incorrect login or password")
	}

	err = s.token.Delete(ctx, user.UserID)
	if err != nil {
		return err
	}

	err = s.repo.DeleteUser(ctx, login)
	if err != nil {
		return err
	}

	return nil
}
