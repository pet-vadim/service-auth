package auth

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

func (s *srv) SignOut(ctx context.Context, refreshToken string) *errs.AppError {
	err := s.token.Delete(ctx, refreshToken)
	if err != nil {
		return err
	}
	return nil
}
