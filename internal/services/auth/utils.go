package auth

import (
	"gitlab.com/pet-vadim/libs/errs"
	"golang.org/x/crypto/bcrypt"
)

const (
	hashCost = 14
)

func HashPassword(password string) (string, *errs.AppError) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), hashCost)
	if err != nil {
		return "", errs.NewUnexpectedError("hash unexpected error")
	}
	return string(bytes), nil
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
