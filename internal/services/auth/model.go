package auth

type User struct {
	UserID       string `db:"id"`
	Email        string `db:"email"`
	PasswordHash string `db:"password_hash"`
}
