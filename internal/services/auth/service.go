package auth

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

//go:generate mockgen -source=./service.go -destination=../../../mocks/service/auth/mockAuthService.go
type Service interface {
	SingIn(ctx context.Context, login, password string) (refreshToken, accessToken string, appErr *errs.AppError)
	SingUp(ctx context.Context, login, password string) (refreshToken, accessToken string, appErr *errs.AppError)
	SignOut(ctx context.Context, refreshToken string) *errs.AppError
	DeleteUser(ctx context.Context, login, password string) *errs.AppError
}
