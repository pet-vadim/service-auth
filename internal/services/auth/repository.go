package auth

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

//go:generate mockgen -source=./repository.go -destination=../../../mocks/repositories/auth/mockAuthRepo.go
type Repository interface {
	DeleteUser(ctx context.Context, login string) *errs.AppError
	GetUser(ctx context.Context, login string) (User, *errs.AppError)
	CreateUser(ctx context.Context, login, passwordHash string) (User, *errs.AppError)
}
