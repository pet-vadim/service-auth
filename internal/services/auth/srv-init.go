package auth

import (
	"gitlab.com/pet-vadim/service-auth/internal/services/token"
)

type srv struct {
	repo  Repository
	token token.Service
}

func New(repo Repository, tokenService token.Service) Service {
	return &srv{
		repo:  repo,
		token: tokenService,
	}
}
