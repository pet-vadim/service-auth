package auth

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
)

func (s *srv) SingIn(ctx context.Context, login, password string) (refreshToken, accessToken string, err *errs.AppError) {
	user, err := s.repo.GetUser(ctx, login)
	if err != nil {
		return "", "", err
	}

	if !CheckPasswordHash(password, user.PasswordHash) {
		logger.Info("failed check password hash")
		return "", "", errs.NewAuthenticationError("incorrect password or login")
	}

	refreshToken, accessToken, err = s.token.Create(ctx, user.UserID)
	if err != nil {
		return "", "", err
	}

	return refreshToken, accessToken, nil
}
