package auth_test

import (
	"context"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/repositories/auth"
	mock_token "gitlab.com/pet-vadim/service-auth/mocks/service/token"
	"testing"
)

func Test_srv_SingIn(t *testing.T) {
	type setMockToken func(s *mock_token.MockService, userID, refreshToken, accessToken string)
	type setMockAuthRepo func(s *mock_auth.MockRepository, login, passwordHash, userID string)
	tests := []struct {
		name            string
		isSuccess       bool
		userID          string
		login           string
		password        string
		passwordHash    string
		refreshToken    string
		accessToken     string
		errMsg          string
		setMockToken    setMockToken
		setMockAuthRepo setMockAuthRepo
	}{
		{// nolint
			name:         "success",
			isSuccess:    true,
			userID:       "1234",
			passwordHash: "$2a$14$ReixcrQTNTtnx05iJ/dcsuOraJsehq3eUH6Zb3Un3dDuz.TkCrtCK",
			refreshToken: "refresh.jwt.example",
			accessToken:  "access.jwt.example",
			login:        "test@mail.ru",
			password:     "Password@1",
			errMsg:       "",
			setMockToken: func(s *mock_token.MockService, userID, refreshToken, accessToken string) {
				s.EXPECT().Create(context.Background(), userID).Return(refreshToken, accessToken, nil).MinTimes(0)
			},
			setMockAuthRepo: func(s *mock_auth.MockRepository, login, passwordHash, userId string) {
				s.EXPECT().GetUser(context.Background(), login).Return(auth.User{
					UserID:       userId,
					Email:        login,
					PasswordHash: passwordHash,
				}, nil)
			},
		},
		{// nolint
			name:         "incorrect password",
			isSuccess:    false,
			userID:       "1234",
			passwordHash: "$2a$14$ReixcrQTNTtnx05iJ/dcsuOraJsehq3eUH6Zb3Un3dDuz.TkCrtCK",
			refreshToken: "refresh.jwt.example",
			accessToken:  "access.jwt.example",
			login:        "test@mail.ru",
			password:     "wrong",
			errMsg:       "incorrect login or password",
			setMockToken: func(s *mock_token.MockService, userID, refreshToken, accessToken string) {
				s.EXPECT().Create(context.Background(), userID).Return(refreshToken, accessToken, nil).MinTimes(0)
			},
			setMockAuthRepo: func(s *mock_auth.MockRepository, login, passwordHash, userId string) {
				s.EXPECT().GetUser(context.Background(), login).Return(auth.User{
					UserID:       userId,
					Email:        login,
					PasswordHash: passwordHash,
				}, nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctr := gomock.NewController(t)
			ctr.Finish()

			token := mock_token.NewMockService(ctr)
			repo := mock_auth.NewMockRepository(ctr)
			tt.setMockToken(token, tt.userID, tt.refreshToken, tt.accessToken)
			tt.setMockAuthRepo(repo, tt.login, tt.passwordHash, tt.userID)
			srv := auth.New(repo, token)

			refreshToken, accessToken, appErr := srv.SingIn(context.Background(), tt.login, tt.password)
			if tt.isSuccess {
				assert.Equal(t, refreshToken, tt.refreshToken)
				assert.Equal(t, accessToken, tt.accessToken)
				assert.Equal(t, nil, appErr)
			}
		})
	}
}
