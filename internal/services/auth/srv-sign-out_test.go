package auth_test

import (
	"context"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/repositories/auth"
	mock_token "gitlab.com/pet-vadim/service-auth/mocks/service/token"
	"testing"
)

func Test_srv_SignOut(t *testing.T) {
	type setMockToken func(s *mock_token.MockService, refreshToken string)
	tests := []struct {
		name         string
		isSuccess    bool
		refreshToken string
		errMsg       string
		setMockToken setMockToken
	}{
		{
			name:         "success",
			isSuccess:    true,
			refreshToken: "refresh.jwt.token",
			errMsg:       "",
			setMockToken: func(s *mock_token.MockService, refreshToken string) {
				s.EXPECT().Delete(context.Background(), refreshToken).Return(nil)
			},
		},
		{
			name:         "invalid token",
			isSuccess:    false,
			refreshToken: "refresh.jwt.token",
			errMsg:       "invalid token",
			setMockToken: func(s *mock_token.MockService, refreshToken string) {
				s.EXPECT().Delete(context.Background(), refreshToken).Return(errs.NewUnexpectedError("invalid token"))
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctr := gomock.NewController(t)
			ctr.Finish()

			token := mock_token.NewMockService(ctr)
			repo := mock_auth.NewMockRepository(ctr)
			tt.setMockToken(token, tt.refreshToken)
			srv := auth.New(repo, token)

			appError := srv.SignOut(context.Background(), tt.refreshToken)
			if tt.isSuccess {
				assert.Equal(t, nil, appError)
				return
			}
			assert.Equal(t, appError.Error(), tt.errMsg)
		})
	}
}
