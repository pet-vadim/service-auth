package token

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
)

//go:generate mockgen -source=./service.go -destination=/home/admin/GolandProjects/pet/service-auth/mocks/service/token/mockTokenService.go
type Service interface {
	Create(ctx context.Context, userID string) (refreshToken, accessToken string, err *errs.AppError)
	Update(ctx context.Context, refreshToken string, accessToken string) (newRefreshToken, newAccessToken string, err *errs.AppError)
	Delete(ctx context.Context, refreshToken string) *errs.AppError
}
