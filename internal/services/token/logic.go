package token

import (
	"context"
	"github.com/golang-jwt/jwt"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"time"
)

const (
	refreshTokenDuration = time.Hour * 240
	accessTokenDuration  = time.Minute * 15
)

type token struct {
	jwtKey []byte
	repo   Repository
}

func New(repo Repository, jwtKey []byte) Service {
	return &token{repo: repo, jwtKey: jwtKey}
}

func (t *token) Create(ctx context.Context, userID string) (refreshToken, accessToken string, appErr *errs.AppError) {
	scRefreshToken := jwt.StandardClaims{
		ExpiresAt: GetExpirationTime(refreshTokenDuration),
		Id:        userID,
	}

	var err error
	refreshToken, err = jwt.NewWithClaims(jwt.SigningMethodHS256, scRefreshToken).SignedString(t.jwtKey)
	if err != nil {
		logger.Error("creating refresh token err: " + err.Error())
		return "", "", errs.NewUnexpectedError("token creating err")
	}

	scReAccessToken := jwt.StandardClaims{
		ExpiresAt: GetExpirationTime(accessTokenDuration),
		Id:        userID,
	}

	accessToken, err = jwt.NewWithClaims(jwt.SigningMethodHS256, scReAccessToken).SignedString(t.jwtKey)
	if err != nil {
		logger.Error("creating access token err: " + err.Error())
		return "", "", errs.NewUnexpectedError("token creating err")
	}

	appErr = t.repo.SaveTokens(ctx, refreshTokenDuration, userID, accessToken, refreshToken)
	if err != nil {
		return "", "", appErr
	}

	return refreshToken, accessToken, nil
}

func (t *token) Update(ctx context.Context, refreshToken, accessToken string) (newRefreshToken, newAccessToken string, appErr *errs.AppError) {
	var claims jwt.StandardClaims
	var err error
	key := t.jwtKey

	tkn, err := jwt.ParseWithClaims(refreshToken, &claims, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		logger.Error("parse token err: " + err.Error())
		return "", "", errs.NewUnexpectedError("unexpected token error")
	}

	if !tkn.Valid {
		logger.Info("token invalid")
		return "", "", errs.NewAuthenticationError("invalid token")
	}

	oldAccessToken, oldRefreshToken, appErr := t.repo.GetTokens(ctx, claims.Id)
	if err != nil {
		return "", "", appErr
	}

	// Токены в базе не совпадают с полученными из jwt, удаляем все токены пользователя из базы
	if refreshToken != oldRefreshToken || accessToken != oldAccessToken {
		appErr = t.repo.DeleteTokens(ctx, claims.Id)
		if err != nil {
			return "", "", appErr
		}
		return "", "", errs.NewAuthenticationError("invalid token")
	}

	// Создаем новые токены
	newRefreshToken, newAccessToken, appErr = t.Create(ctx, claims.Id)
	if err != nil {
		return "", "", appErr
	}

	return newRefreshToken, newAccessToken, nil
}

func (t *token) Delete(ctx context.Context, refreshToken string) *errs.AppError {
	var claims jwt.StandardClaims
	key := t.jwtKey

	tkn, err := jwt.ParseWithClaims(refreshToken, &claims, func(t *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		logger.Error("err during parsing token")
		return errs.NewUnexpectedError("invalid token")
	}

	if !tkn.Valid {
		logger.Info("invalid token")
		return errs.NewAuthenticationError("invalid token")
	}

	appErr := t.repo.DeleteTokens(ctx, claims.Id)
	if appErr != nil {
		return appErr
	}

	return nil
}

func GetExpirationTime(duration time.Duration) (unix int64) {
	return time.Now().Add(duration).Unix()
}
