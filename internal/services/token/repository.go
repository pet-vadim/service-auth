package token

import (
	"context"
	"gitlab.com/pet-vadim/libs/errs"
	"time"
)

type Repository interface {
	GetTokens(ctx context.Context, userID string) (accessToken, refreshToken string, err *errs.AppError)
	SaveTokens(ctx context.Context, expiration time.Duration, userID, accessToken, refreshToken string) *errs.AppError
	DeleteTokens(ctx context.Context, userID string) *errs.AppError
}
