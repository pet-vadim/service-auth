package tokenrepo

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-auth/internal/services/token"
	"time"
)

const (
	keyAccessToken  = "accessToken"
	keyRefreshToken = "refreshToken"
)

type rdb struct {
	client *redis.Client
}

func New(db *redis.Client) token.Repository {
	return &rdb{client: db}
}

func (r *rdb) GetTokens(ctx context.Context, userID string) (accessToken, refreshToken string, appErr *errs.AppError) {
	res, err := r.client.HMGet(ctx, userID).Result()
	if err != nil {
		logger.Error("getting tokens err: " + err.Error())
		return "", "", errs.NewUnexpectedError("database unexpected error")
	}
	requiredLength := 4
	if len(res) != requiredLength {
		logger.Error("not all necessary tokens are available in the storage")
		return "", "", errs.NewUnexpectedError("database unexpected error")
	}

	accessToken, okAcc := res[1].(string)
	refreshToken, okRef := res[3].(string)
	if !okAcc || !okRef {
		logger.Error("extracting token err")
		return "", "", errs.NewUnexpectedError("can not extract tokens from storage")
	}

	return accessToken, refreshToken, nil
}

func (r *rdb) SaveTokens(ctx context.Context, expiration time.Duration, userID, accessToken, refreshToken string) *errs.AppError {
	var err error
	_, err = r.client.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
		err = pipe.HSet(ctx, userID, keyAccessToken, accessToken, keyRefreshToken, refreshToken).Err()
		if err != nil {
			return err
		}
		err = pipe.Expire(ctx, userID, expiration).Err()
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		logger.Error("save token err" + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	return nil
}

func (r *rdb) DeleteTokens(ctx context.Context, userID string) *errs.AppError {
	err := r.client.Del(ctx, userID).Err()
	if err != nil {
		logger.Error("delete tokens err: " + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}
	return nil
}
