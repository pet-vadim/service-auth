package repositories

import (
	"context"
	"github.com/go-redis/redis/v8"
	"gitlab.com/pet-vadim/libs/logger"
)

type RedisConfig struct {
	Addr     string
	Password string
	DB       int
}

func NewRedisDB(c *RedisConfig) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     c.Addr,
		Password: c.Password,
		DB:       c.DB,
	})
	_, err := client.Ping(context.Background()).Result()
	if err != nil {
		logger.Fatal("redis connection ping failed" + err.Error())
	}

	return client
}
