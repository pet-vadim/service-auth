package authrepo

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
)

type repository struct {
	db *sqlx.DB
}

func New(postgresDB *sqlx.DB) auth.Repository {
	return &repository{db: postgresDB}
}

func (r *repository) DeleteUser(ctx context.Context, login string) *errs.AppError {
	query := `
			 DELETE 
			 FROM users
			 WHERE email=$1;
			 `

	res, err := r.db.ExecContext(ctx, query, login)
	if err != nil {
		logger.Error("err while deleting user from database: " + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	count, err := res.RowsAffected()
	if err != nil {
		logger.Error("user was not found while deleting from database: " + err.Error())
		return errs.NewUnexpectedError("database unexpected error")
	}

	if count == 0 {
		logger.Error("user was not found while deleting from database, row affected == 0")
		return errs.NewUnexpectedError("database unexpected error")
	}

	return nil
}

func (r *repository) GetUser(ctx context.Context, login string) (auth.User, *errs.AppError) {
	var user auth.User
	query := `
			 SELECT id, email, password_hash
			 FROM users
			 WHERE email=$1;
			 `

	err := r.db.GetContext(ctx, &user, query, login)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Info("user was not found in db")
			return auth.User{}, errs.NewNotFoundError("user was not found")
		}
		logger.Error("err during getting user from db: " + err.Error())
		return auth.User{}, errs.NewUnexpectedError("database unexpected error")
	}

	return user, nil
}

func (r *repository) CreateUser(ctx context.Context, login, passwordHash string) (auth.User, *errs.AppError) {
	query := `
			 INSERT INTO users
			 VALUES (gen_random_uuid(), $1, $2)
			 RETURNING id, email, password_hash
			 `

	rowx := r.db.QueryRowxContext(ctx, query, login, passwordHash)

	var user auth.User
	err := rowx.StructScan(&user)
	if err != nil {
		logger.Error("err during struct scan from db row: " + err.Error())
		return auth.User{}, errs.NewUnexpectedError("database unexpected err")
	}

	return user, nil
}
