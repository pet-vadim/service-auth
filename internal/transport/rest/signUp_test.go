package rest_test

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/service-auth/internal/transport/rest"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/service/auth"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_signUp(t *testing.T) {
	var tests = []struct {
		name               string
		isSuccess          bool
		email              string
		password           string
		expectedStatusCode int
		expectedErr        string
	}{
		{
			name:               "success",
			isSuccess:          true,
			email:              "test@mail.ru",
			password:           "Password@mail.ru",
			expectedStatusCode: http.StatusOK,
			expectedErr:        "",
		},
		{
			name:               "fail email binding",
			isSuccess:          false,
			email:              "",
			password:           "invalidemail@asdf",
			expectedStatusCode: http.StatusUnprocessableEntity,
			expectedErr:        "email and password fields are required",
		},
		{
			name:               "fail email binding",
			isSuccess:          false,
			email:              "",
			password:           "Password1@",
			expectedStatusCode: http.StatusUnprocessableEntity,
			expectedErr:        "email and password fields are required",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			defer ctr.Finish()

			auth := mock_auth.NewMockService(ctr)
			signUpGenerateMock(auth, tt.email, tt.password)
			handler := rest.New(auth, &rest.Config{})

			gin.SetMode(gin.TestMode)
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			b, err := json.Marshal(rest.SignUpInput{
				Email:    tt.email,
				Password: tt.password,
			})
			if err != nil {
				t.Fatal(err.Error())
			}
			c.Request = httptest.NewRequest(http.MethodPost, "/sign-up", bytes.NewBuffer(b))

			// Act
			handler.SignUp(c)

			// Assert
			if tt.isSuccess {
				var output rest.SignUpOutput
				err = json.Unmarshal(w.Body.Bytes(), &output)
				if err != nil {
					t.Fatal(err.Error())
				}
				assert.MatchRegex(t, regexJwt, output.AccessToken)
			} else {
				var errOutput rest.ErrResponse
				err = json.Unmarshal(w.Body.Bytes(), &errOutput)
				if err != nil {
					t.Fatal(err.Error())
				}
				assert.Equal(t, tt.expectedErr, errOutput.Error)
			}
			assert.Equal(t, tt.expectedStatusCode, w.Code)
		})
	}
}

func signUpGenerateMock(s *mock_auth.MockService, login, password string) {
	s.EXPECT().SingUp(context.Background(), login, password).MinTimes(0)
}
