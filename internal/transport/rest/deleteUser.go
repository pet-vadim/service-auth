package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"net/http"
)

type DeleteUserInput struct {
	Email    string `json:"email" binding:"required" example:"test@mail.ru"`
	Password string `json:"password" binding:"required" example:"qwerty"`
}

type DeleteUserOutput struct {
	Status string `json:"status"`
}

// DeleteUser
// @Summary Delete User
// @Security ApiKeyAuth
// @Tags Auth
// @Description Delete existing user. Required login and password regardless of the tokens
// @Accept json
// @Produce json
// @Param input body DeleteUserInput true "valid email and password of user"
// @Success 200 {object} DeleteUserOutput "success"
// @Failure 422 {object} ErrResponse "incorrect struct of request or validation failed"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /authentication/delete-user [delete]
func (h *Handler) DeleteUser(c *gin.Context) {
	var input DeleteUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		logger.Error("err binding json: " + err.Error())
		SendErrResp(c, errs.NewUnprocessableEntity("fields email and password is required"))
		return
	}

	ctx := c.Request.Context()
	appErr := h.Service.DeleteUser(ctx, input.Email, input.Password)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	c.SetCookie(RefreshJWT, "", 0, "/", "localhost", false, true)

	c.JSON(http.StatusOK, DeleteUserOutput{
		Status: "delete success",
	})
}
