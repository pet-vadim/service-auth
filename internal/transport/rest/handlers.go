package rest

import (
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/pet-vadim/service-auth/docs"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
	"net/http"
)

type Handler struct {
	Service auth.Service
	Config  *Config
}

func New(s auth.Service, c *Config) *Handler {
	return &Handler{Service: s, Config: c}
}

func (h *Handler) InitRoutes() *gin.Engine {
	gin.SetMode(h.Config.GinMode)

	r := gin.New()

	r.Use(gin.Recovery())
	r.Use(JSONLogMiddleware())
	r.GET("/authentication/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	r.GET("/health", func(c *gin.Context) {
		c.String(http.StatusOK, "I am alive")
	})

	authentication := r.Group("/authentication")
	{ //nolint
		authentication.POST("/sign-in", h.SignIn)
		authentication.POST("/sign-up", h.SignUp)
		authentication.GET("/sign-out", h.CheckAuth, h.SignOut)
		authentication.DELETE("/delete-user", h.CheckAuth, h.DeleteUser)
	}

	return r
}

type Config struct {
	Protocol string
	Host     string
	Port     string
	GinMode  string
	LogLvl   string
	Origin   string
	JWTKey   []byte
}
