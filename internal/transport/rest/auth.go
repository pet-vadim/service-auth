package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"strings"
)

const (
	authorizationHeader = "Authorization"
)

func (h Handler) CheckAuth(c *gin.Context) {
	header := c.GetHeader(authorizationHeader)
	if header == "" {
		// Логи в режиме Info так как это штатная ситуация
		logger.Info("empty auth header")
		AbortWithErr(c, errs.NewAuthorizationError("empty auth header"))
		return
	}

	headerParts := strings.Split(header, " ")
	if len(headerParts) != 2 || headerParts[0] != "Bearer" {
		logger.Info("invalid auth header")
		AbortWithErr(c, errs.NewAuthorizationError("invalid auth header"))
		return
	}

	accessJWT := headerParts[1]
	if accessJWT == "" {
		logger.Info("token is empty")
		AbortWithErr(c, errs.NewAuthorizationError("token is empty"))
		return
	}

	var claims jwt.StandardClaims
	token, err := jwt.ParseWithClaims(accessJWT, &claims, func(token *jwt.Token) (interface{}, error) {
		return h.Config.JWTKey, nil
	})
	if err != nil {
		logger.Error("parse token err" + err.Error())
		AbortWithErr(c, errs.NewAuthorizationError("unexpected token error"))
		return
	}

	if !token.Valid {
		logger.Info("invalid token")
		AbortWithErr(c, errs.NewAuthorizationError("invalid token"))
		return
	}

	SetUserID(c, claims.Id)
	c.Next()
}

// Логика SetUserID и GetUserID вынесена, так как переиспользуется в разных частях

func SetUserID(c *gin.Context, id string) {
	c.Set(userID, id)
}

func GetUserID(c *gin.Context) string {
	var userIDInterface, _ = c.Get(userID)
	resUserID, ok := userIDInterface.(string)
	if !ok {
		return ""
	}
	return resUserID
}
