package rest

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
)

// SignOut
// @Summary Sign out
// @Security ApiKeyAuth
// @Tags Auth
// @Description Sign out for existed user
// @Success 200 {string} string "success"
// @Failure 401 {object} ErrResponse "unauthorized"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /auth/sign-out [get]
func (h *Handler) SignOut(c *gin.Context) {
	refreshJWT, err := c.Cookie(RefreshJWT)
	if err != nil {
		logger.Error("get refreshJWT from cookies: " + err.Error())
		SendErrResp(c, errs.NewUnexpectedError("token was not found"))
		return
	}
	fmt.Println("refreshJWT", refreshJWT)

	ctx := c.Request.Context()
	appErr := h.Service.SignOut(ctx, refreshJWT)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}
	c.SetCookie(refreshJWT, "", 0, "/", "localhost", false, true)
}
