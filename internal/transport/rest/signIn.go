package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"net/http"
)

type SignInInput struct {
	Email    string `json:"email" binding:"required" example:"test@mail.ru"`
	Password string `json:"password" binding:"required" example:"Password1@"`
}

type SignInOutput struct {
	AccessToken string `json:"access_token"`
}

// SignIn
// @Summary Sign in
// @Tags Auth
// @Description Sign in for existed user (refresh token in httponly and access token in json)
// @Accept json
// @Produce json
// @Param credentials body SignInInput true "valid email and password of existed user"
// @Success 200 {object} SignInOutput "success"
// @Failure 401 {object} ErrResponse "login or password is incorrect"
// @Failure 422 {object} ErrResponse "incorrect struct of request or validation failed"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /authentication/sign-in [post]
func (h *Handler) SignIn(c *gin.Context) {
	ctx := c.Request.Context()
	var input SignInInput

	err := c.ShouldBindJSON(&input)
	if err != nil {
		logger.Error("binding json err: " + err.Error())
		SendErrResp(c, errs.NewUnprocessableEntity("email and password fields are required"))
		return
	}

	refreshToken, accessToken, appErr := h.Service.SingIn(ctx, input.Email, input.Password)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	c.SetCookie(RefreshJWT, refreshToken, -1, "/", "localhost", false, true)
	c.JSON(http.StatusOK, SignInOutput{AccessToken: accessToken})
}
