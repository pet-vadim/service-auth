package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
	"gitlab.com/pet-vadim/libs/logger"
	"net/http"
)

const RefreshJWT = "REFRESH_JWT"

type SignUpInput struct {
	Email    string `json:"email" binding:"required" example:"test@mail.ru"`
	Password string `json:"password" binding:"required" example:"qwerty" minLength:"6"`
}

type SignUpOutput struct {
	AccessToken string `json:"access_token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"` //nolint:lll
}

// SignUp
// @Summary Sign up
// @Tags Auth
// @Description Sign up for new user (refresh token in the httponly cookies and access token in json)
// @Accept json
// @Produce json
// @Param credentials body SignUpInput true "valid email and password of new user"
// @Success 200 {object} SignUpOutput "success"
// @Failure 422 {object} ErrResponse "incorrect struct of request or validation failed"
// @Failure 500 {object} ErrResponse "internal server error"
// @Router /authentication/sign-up [post]
func (h *Handler) SignUp(c *gin.Context) {
	var input SignUpInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		logger.Error("bind json error: " + err.Error())
		SendErrResp(c, errs.NewUnprocessableEntity("email and password fields are required"))
		return
	}

	ctx := c.Request.Context()
	refreshToken, accessToken, appErr := h.Service.SingUp(ctx, input.Email, input.Password)
	if appErr != nil {
		SendErrResp(c, appErr)
		return
	}

	c.SetCookie(RefreshJWT, refreshToken, -1, "/", "localhost", false, true)
	c.JSON(http.StatusOK, SignUpOutput{AccessToken: accessToken})
}
