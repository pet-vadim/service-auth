package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/errs"
)

type ErrResponse struct {
	Error string `json:"error" example:"user definition of error"`
}

func SendErrResp(c *gin.Context, err *errs.AppError) {
	c.JSON(err.Code, ErrResponse{
		Error: err.Error(),
	})
}

func AbortWithErr(c *gin.Context, err *errs.AppError) {
	c.AbortWithStatusJSON(err.Code, ErrResponse{
		Error: err.Message,
	})
}
