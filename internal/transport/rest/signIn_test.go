package rest_test

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/service-auth/internal/transport/rest"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/service/auth"
	"net/http"
	"net/http/httptest"
	"testing"
)

const regexJwt = "^[\\w-]*\\.[\\w-]*\\.[\\w-]*$"

func TestHandler_signIn(t *testing.T) {
	tests := []struct {
		name               string
		isSuccess          bool
		login              string
		password           string
		expectedStatusCode int
		errMsg             string
	}{
		{
			name:               "success",
			isSuccess:          true,
			login:              "test@mail.ru",
			password:           "Password1A",
			expectedStatusCode: 200,
			errMsg:             "",
		},
		{
			name:               "fail no password",
			isSuccess:          false,
			login:              "test@mail.ru",
			password:           "",
			errMsg:             "email and password fields are required",
			expectedStatusCode: http.StatusUnprocessableEntity,
		},
		{
			name:               "fail no mail",
			isSuccess:          false,
			login:              "",
			password:           "Passwrord1@",
			errMsg:             "email and password fields are required",
			expectedStatusCode: http.StatusUnprocessableEntity,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			ctr.Finish()

			auth := mock_auth.NewMockService(ctr)
			SignInGenerateMock(auth, tt.login, tt.password)
			handler := rest.New(auth, &rest.Config{})

			gin.SetMode(gin.TestMode)
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			b, err := json.Marshal(rest.SignInInput{Email: tt.login, Password: tt.password})
			if err != nil {
				t.Fatal(err.Error())
			}
			c.Request = httptest.NewRequest(http.MethodPost, "/sign-in", bytes.NewBuffer(b))

			// Act
			handler.SignIn(c)

			// Assert
			assert.Equal(t, tt.expectedStatusCode, w.Code)

			if tt.isSuccess {
				cookies := w.Result().Cookies() //nolint:bodyclose
				if len(cookies) == 0 {
					t.Fatal("no cookies")
				}
				refreshJWT := cookies[0].Value

				var output rest.SignInOutput
				err = json.Unmarshal(w.Body.Bytes(), &output)
				if err != nil {
					t.Fatal(err.Error())
				}

				assert.MatchRegex(t, refreshJWT, regexJwt)
				assert.MatchRegex(t, output.AccessToken, regexJwt)
			} else {
				var output rest.ErrResponse
				err = json.Unmarshal(w.Body.Bytes(), &output)
				if err != nil {
					t.Fatal(err.Error())
				}
				assert.Equal(t, output.Error, tt.errMsg)
			}
		})
	}
}

func SignInGenerateMock(s *mock_auth.MockService, login, password string) {
	s.
		EXPECT().
		SingIn(context.Background(), login, password).
		Return("refresh.jwt.example", "access.jwt.example", nil).
		MinTimes(0)
}
