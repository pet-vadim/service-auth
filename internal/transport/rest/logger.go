package rest

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pet-vadim/libs/logger"
	"strings"
	"time"
)

var userID = "userID"

func JSONLogMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Start timer
		start := time.Now()

		// Process Request
		c.Next()

		logger.Middleware(
			c.Request.Method,
			c.Request.RequestURI,
			GetUserID(c),
			getClientIP(c),
			c.Writer.Status(),
			getDurationInMilliseconds(start),
		)
	}
}

func getDurationInMilliseconds(start time.Time) time.Duration {
	end := time.Now()
	duration := end.Sub(start)
	milliseconds := float64(duration) / float64(time.Millisecond)
	return time.Duration(milliseconds)
}

// getClientIP gets the correct IP for the end client instead of the proxy
func getClientIP(c *gin.Context) string {
	// first check the X-Forwarded-For header
	requester := c.Request.Header.Get("X-Forwarded-For")
	// if empty, check the Real-IP header
	if requester == "" {
		requester = c.Request.Header.Get("X-Real-IP")
	}
	// if the requester is still empty, use the hard-coded address from the socket
	if requester == "" {
		requester = c.Request.RemoteAddr
	}

	// if requester is a comma delimited list, take the first one
	// (this happens when proxied via elastic load balancer then again through nginx)
	if strings.Contains(requester, ",") {
		requester = strings.Split(requester, ",")[0]
	}

	return requester
}
