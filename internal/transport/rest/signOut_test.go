package rest_test

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	"gitlab.com/pet-vadim/service-auth/internal/transport/rest"
	mock_auth "gitlab.com/pet-vadim/service-auth/mocks/service/auth"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_signOut(t *testing.T) {
	tests := []struct {
		name               string
		isSuccess          bool
		cookieName         string
		jwt                string
		expectedStatusCode int
	}{
		{
			name:               "success",
			isSuccess:          true,
			cookieName:         rest.RefreshJWT,
			jwt:                "example.jwt.token",
			expectedStatusCode: http.StatusOK,
		},
		{
			name:               "wrong jwt name",
			isSuccess:          false,
			cookieName:         "err",
			jwt:                "example.jwt.token",
			expectedStatusCode: http.StatusInternalServerError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			ctr := gomock.NewController(t)
			ctr.Finish()

			auth := mock_auth.NewMockService(ctr)
			signOutGenerateMock(auth, tt.jwt)
			handler := rest.New(auth, &rest.Config{})

			gin.SetMode(gin.TestMode)
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			c.Request = httptest.NewRequest(http.MethodGet, "/sign-out", http.NoBody)
			cookie := http.Cookie{
				Name:  tt.cookieName,
				Value: tt.jwt,
			}
			c.Request.AddCookie(&cookie)

			// Act
			handler.SignOut(c)

			// Assert
			cookies := w.Result().Cookies() //nolint:bodyclose
			if tt.isSuccess {
				assert.Equal(t, 1, len(cookies))
			}
			assert.Equal(t, tt.expectedStatusCode, w.Code)
		})
	}
}

func signOutGenerateMock(s *mock_auth.MockService, jwt string) {
	s.EXPECT().SignOut(context.Background(), jwt).Return(nil).MinTimes(0)
}
