FROM golang:1.17 as BUILDER

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN go get github.com/swaggo/echo-swagger
RUN go install github.com/swaggo/swag/cmd/swag@v1.7.9-p1
RUN swag init -g cmd/main.go
RUN CGO_ENABLED=0 GOOS=linux go build -o app cmd/main.go

FROM alpine:3.15 AS prodaction
COPY --from=builder /app .
CMD ["./app"]