package main

import (
	"context"
	"fmt"
	"gitlab.com/pet-vadim/libs/logger"
	"gitlab.com/pet-vadim/service-auth/internal/repositories"
	"gitlab.com/pet-vadim/service-auth/internal/repositories/authrepo"
	"gitlab.com/pet-vadim/service-auth/internal/repositories/tokenrepo"
	"gitlab.com/pet-vadim/service-auth/internal/services/auth"
	"gitlab.com/pet-vadim/service-auth/internal/services/token"
	"gitlab.com/pet-vadim/service-auth/internal/transport/rest"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

// @title Auth Service
// @version 1.0
// @description API server jwt Authentication

// @host localhost:8080
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	logger.Info("Starting app")

	redisDB := repositories.NewRedisDB(getRedisConfig())
	postgresDB := repositories.NewPostgresDB(getPostgresConfig())

	tokenRepo := tokenrepo.New(redisDB)
	tokenService := token.New(tokenRepo, getJWTKey())

	authRepo := authrepo.New(postgresDB)
	authService := auth.New(authRepo, tokenService)
	handler := rest.New(authService, getHandlersConfig())

	srv := &http.Server{
		Addr:    ":" + handler.Config.Port,
		Handler: handler.InitRoutes(),
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Fatal("server listen error: " + err.Error())
		}
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logger.Info("received os.signal: " + (<-quit).String())

	// The context is used to inform the server it has second to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Fatal("server forced to shutdown: " + err.Error())
	}

	if err := postgresDB.Close(); err != nil {
		logger.Fatal("fail db connection close " + err.Error())
	}

	if err := redisDB.Close(); err != nil {
		logger.Fatal("fail db connection close " + err.Error())
	}

	logger.Info("server exiting")
}

func getPostgresConfig() *repositories.PostgresConfig {
	return &repositories.PostgresConfig{
		DBUsername: os.Getenv("DB_USERNAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBTable:    os.Getenv("DB_TABLE"),
	}
}

func getRedisConfig() *repositories.RedisConfig {
	return &repositories.RedisConfig{
		Addr: os.Getenv("RDB_ADDR"),
		DB:   toInt(os.Getenv("RDB_DB")),
	}
}

func getHandlersConfig() *rest.Config {
	return &rest.Config{
		Protocol: os.Getenv("SRV_PROTOCOL"),
		Host:     os.Getenv("SRV_HOST"),
		Port:     os.Getenv("SRV_PORT"),
		GinMode:  os.Getenv("SRV_GIN_MODE"),
		LogLvl:   os.Getenv("SRV_LOG_LVL"),
		JWTKey:   getJWTKey(),
		Origin: fmt.Sprintf(
			"%v://%v:%v/",
			os.Getenv("SRV_PROTOCOL"),
			os.Getenv("SRV_HOST"),
			os.Getenv("SRV_PORT")),
	}
}

func getJWTKey() []byte {
	return []byte(os.Getenv("JWT_KEY"))
}

func toInt(v string) int {
	i, err := strconv.Atoi(v)
	if err != nil {
		logger.Fatal("fail convert env to bool")
	}
	return i
}
