generate-mock:
	go generate ./...

start-unit-test: generate-mock
	go test ./...

swag-init:
	swag init -g cmd/main.go

lint:
	golangci-lint run

start:
	go build cmd/main.go
	./main